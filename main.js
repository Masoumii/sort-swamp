$(document).ready(function(){
    
    /* Fade-in the body on load */
    $("body").hide().fadeIn(1000);
    
    /* Apply animation on the top 3 players in the scoreboard page */
    $('table tr:nth-child(2), table tr:nth-child(3), table tr:nth-child(4)').addClass('animated pulse infinite');

    /* Initial score */
    var score = 0;
    /* Initial total time */
    var totalTime = 60;


  
    
    /* Animation function: BounceIn */
    function bounceInAnimate(obj){
        $(obj).addClass("animated bounceIn");
        setTimeout(function(){ $(obj).removeClass("animated bounceIn"); }, 1000);
    }
    
    /* Animation function: Flip */
    function flipAnimate(obj){
        $(obj).addClass('animated flip');
         setTimeout(function(){ $(obj).removeClass("animated flip"); }, 2000);
    }
    
    
    /* Animation function: Rotate */
    function rotateOutAnimate(obj){
         $(obj).addClass('animated rotateOut');
         setTimeout(function(){ $(obj).removeClass("animated rotateOut"); }, 2000);
    }
    
     /* Animation function: Pulse */
    function pulseAnimate(obj){
         $(obj).addClass('animated pulse');
         setTimeout(function(){ $(obj).removeClass("animated pulse"); }, 2000);
    }
    
    /* Countdown Function */
    function countDown() {
        
        /* Decrease time by 1 seconds */
        totalTime -= 1;
        
        /* Update the time on the screen */
        $("#time").text(totalTime);
        
        /* Game-Over if time is over */
        if(totalTime === 0){
            gameOver();
        }
        
        /* Make the color of the time text red if at 10 (or lower) seconds */
        if(totalTime <= 10){
            $("#time").css("color","red");
            flipAnimate("#time");
        }
        
        /* Disable Help function if time is less than 6 sec */
        if(totalTime < 6){
             $("#helpUser").hide();
        }
        /* Enable Help function if time is more than 5 sec */
        if(totalTime > 5){
             $("#helpUser").show();
        }
}

/* Function Game Over */
    function gameOver(){
        var countDownInterval = setInterval(countDown, 1000);
         clearInterval(countDownInterval);
         playerSteps = [];
        gameSteps = [];
        clearWindow();
        boxExplosion.play(); 
        backgroundMusic.pause();
        backgroundMusic.currentTime = 0;
        $(".container, #images, #example").hide();
        $("#gameover-window").show();
        bounceInAnimate("#gameover-window");
        $("#score").val(score);
        highscorePage = true;
    }
    
    /* Hide everything and show start button on load */
    $("#images, .container, #example").hide();
    var currentLevel = 1;

    /* UI Variables: Buttons */ 
    var crab = document.getElementById("newCrab");
    var cat = document.getElementById("newCat");
    var fish = document.getElementById("newFish");
    var fox = document.getElementById("newFox");
    
    /* UI Variables: Windows */
    var clearBtn = document.getElementById("clearWindow");
    var helpBtn = document.getElementById("helpUser");
    var gameStartBtn = document.getElementById("start-game");
    var exampleWindow = document.getElementById("example");

    /* Audio Variables */
    var hoverSound = new Audio('audio/hover.wav');
    var boxDrop = new Audio('audio/box-drop.mp3');
    var boxClear = new Audio('audio/wipe.mp3');
    var boxExplosion = new Audio('audio/explosion.mp3');
    var lightning = new Audio("audio/lightning.mp3");
    var winnerMusic = new Audio("audio/winner-music.mp3");
    var helpMusic = new Audio("audio/help.mp3");
    var backgroundMusic = new Audio("audio/background-music.mp3");
    
    /* Sorting-order Steps Variables */
    var possibleSteps = ["Cat", "Fish", "Crab", "Fox"];
    var playerSteps = [];
    var gameSteps = [];
    
        
        /* Start the game */
    function gameStart(){  
        setInterval(countDown, 1000);
        backgroundMusic.play();
        $("#start-window, .info-section, .choice-section").hide();
        $("#images, #example, .container").slideDown(1000,function(){
        }); 
        generate_level(2);
        $(exampleWindow).text("LEVEL 1: \n"+gameSteps).hide().fadeIn().delay(5000).fadeOut();
    };

       /* Generate level */
   function generate_level(loopAmount){  
        for(i=1; i<=loopAmount ;i++){
                var randomBox = possibleSteps[Math.floor(Math.random() * possibleSteps.length)];
                gameSteps.push(randomBox);
            }
   }

   /* Function to add an animal to the sorting-window */
    function addAnimal(animal){
        boxDrop.play();
        var animalClass = "box"+" "+animal.toLowerCase();
        var box = $("<div class='"+animalClass+"'></div>");
        $(".container").append(box);
        $(box).hide().fadeIn(500);
        playerSteps.push(animal);
    }

    /* Function to clear the sorting window */
    function clearWindow(){
        boxClear.play();
        $(".box").fadeOut(500);
    }
    
    /* Functie to show user a Hint/Help */
    function showHelp(){
        
        if(totalTime > 5){
            
        $("#time").addClass("red");
        setTimeout(function(){ $("#time").removeClass("red"); }, 1500);
            
        $("#helpUser, #example, #time").addClass("animated bounceIn");
        setTimeout(function(){ $("#helpUser, #example, #time").removeClass("animated bounceIn"); }, 2000);
           
        helpMusic.play();
        totalTime -= 5;
        $("#example").finish();
        $("#example").fadeIn().delay(5000).fadeOut();
        }
        
    }

    /* Runs when a level/round is completed */
    function wonRound(){
        bounceInAnimate("#time");
        
        $(".box").addClass("scaleOut");
        setTimeout(function(){ $(".box").removeClass("scaleOut"); }, 2000);
        
        $("#time").addClass("green");
        setTimeout(function(){ $("#time").removeClass("green"); }, 2000);
        
        playerSteps = [];
        gameSteps = [];
        clearWindow();
        boxExplosion.play(); 
        currentLevel +=1;
        score += 10;
        totalTime += 5;

            /* LEVEL 2 */
            if(currentLevel === 2){
                setTimeout(
                    function() 
                    {
                        generate_level(3);
                        $(exampleWindow).text("LEVEL 2: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750);  
            };

            /* LEVEL 3 */
            if(currentLevel === 3){
                setTimeout(
                    function() 
                    {
                        generate_level(4);
                        $(exampleWindow).text("LEVEL 3: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            }

            /* LEVEL 4 */
            if(currentLevel === 4){
                setTimeout(
                    function() 
                    {
                        generate_level(5);
                        $(exampleWindow).text("LEVEL 4: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            }
            
            /* LEVEL 5 */
            if(currentLevel === 5){
                setTimeout(
                    function() 
                    {
                        generate_level(5);
                        $(exampleWindow).text("LEVEL 5: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            } 
            
            /* LEVEL 6 */
            if(currentLevel === 6){
                setTimeout(
                    function() 
                    {
                        generate_level(5);
                        $(exampleWindow).text("LEVEL 6: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            } 
            
            /* LEVEL 7 */
            if(currentLevel === 7){
                setTimeout(
                    function() 
                    {
                        generate_level(6);
                        $(exampleWindow).text("LEVEL 7: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            } 
            
             /* LEVEL 8 */
            if(currentLevel === 8){
                setTimeout(
                    function() 
                    {
                        generate_level(7);
                        $(exampleWindow).text("LEVEL 8: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            } 
            
            /* LEVEL 9 */
            if(currentLevel === 9){
                setTimeout(
                    function() 
                    {
                        generate_level(7);
                        $(exampleWindow).text("LEVEL 9: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            } 
            
            /* LEVEL 10 */
            if(currentLevel === 10){
                setTimeout(
                    function() 
                    {
                        generate_level(8);
                        $(exampleWindow).text("LEVEL 10: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            } 
            
            /* LEVEL 11 */
            if(currentLevel === 11){
                setTimeout(
                    function() 
                    {
                        generate_level(8);
                        $(exampleWindow).text("LEVEL 11: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            } 
            
            /* LEVEL 12 */
            if(currentLevel === 12){
                setTimeout(
                    function() 
                    {
                        generate_level(9);
                        $(exampleWindow).text("LEVEL 12: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            } 
            
             /* LEVEL 13 */
            if(currentLevel === 13){
                setTimeout(
                    function() 
                    {
                        generate_level(9);
                        $(exampleWindow).text("LEVEL 13: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            }
            
            
            /* LEVEL 14 */
            if(currentLevel === 14){
                setTimeout(
                    function() 
                    {
                        generate_level(9);
                        $(exampleWindow).text("LEVEL 14: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            }
            
            
            /* LEVEL 15 */
            if(currentLevel === 15){
                setTimeout(
                    function() 
                    {
                        generate_level(9);
                        $(exampleWindow).text("LEVEL 15: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            }
            
            
             /* LEVEL 16 */
            if(currentLevel === 16){
                setTimeout(
                    function() 
                    {
                        generate_level(10);
                        $(exampleWindow).text("LEVEL 16: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            }
            
            /* LEVEL 17 */
            if(currentLevel === 17){
                setTimeout(
                    function() 
                    {
                        generate_level(10);
                        $(exampleWindow).text("LEVEL 17: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            }
            
            /* LEVEL 18 */
            if(currentLevel === 18){
                setTimeout(
                    function() 
                    {
                        generate_level(10);
                        $(exampleWindow).text("LEVEL 18: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            }
            
            /* LEVEL 19 */
            if(currentLevel === 19){
                setTimeout(
                    function() 
                    {
                        generate_level(10);
                        $(exampleWindow).text("LEVEL 19: \n "+gameSteps).hide().fadeIn().delay(5000).fadeOut();
                    }, 750); 
            }


            /* LEVEL 20   */
            if(currentLevel === 20){
                setTimeout(
                    function() 
                    {
                        alert("Speel uitgespeeld!");
                        backgroundMusic.pause();
                        winnerMusic.play();
                        $("#images, .container, .example").hide();
                        $(".winner-text").fadeIn(3000);
                        $(".winner-img").fadeIn();
                         $("#gameover-window").fadeIn(3000);
                         score += 10;
                          $("#score").val(score);
                    }, 750); 
            }
    }

    /* Checks for wins at every click on any animal */
    function checkSteps(){
            
            /* Array to string */
            a = playerSteps.toString();
            b = gameSteps.toString();
            
            /* Compare player's array against the game's generated array and run the Win-function if they're equal */
            if(a === b){
                wonRound();
            }
        
    }

    /* Event Listeners: clicks */
   $(crab).click(function(){
    addAnimal("Crab");
    checkSteps();
    bounceInAnimate(this);
   });

   $(cat).click(function(){
    addAnimal("Cat");
    checkSteps();
    bounceInAnimate(this);
   });

   $(fish).click(function(){
    addAnimal("Fish");
    checkSteps();
    bounceInAnimate(this);
   });
   
   $(fox).click(function(){
    addAnimal("Fox");
    checkSteps();
    bounceInAnimate(this);
   });

   $(clearBtn).click(function(){
    clearWindow();
    playerSteps = [];
    bounceInAnimate(this);
   });
   
    $(helpBtn).click(function(){
    showHelp();
   });

   $(gameStartBtn).click(function(){
       gameStart();
   });
   
    $("#hall-of-fame").click(function(){
       boxDrop.play();
        $("body").fadeOut(750, function() {
            window.location.replace("scoreboard.php");
        });
       
   });
   
   $("#start-game").click(function(){
       boxDrop.play();
   });
   
   $(".start-text").mouseenter(function(){
       hoverSound.play();
   });
   
    $("#explode").click(function(){
        flipAnimate(".animals, #example");
        lightning.play();
        $(".box, #explode").fadeOut();       
        wonRound();
   });
   
   $(".back-link").click(function(){
       boxDrop.play();
        $("body").fadeOut(750, function() {
            window.location.replace("index.php");
        });
   });
   
   
   
   /* PHONE FUNCTIONS */
   /* Normal vibrate 1x */
   $("#newCrab, #newFox, #newFish, #newCat, #clearWindow, .start-text, .back-link").click(function(){
       navigator.vibrate(10); 
   });
   
   /* Explode vibrate 2x*/
   $("#explode").click(function(){
       navigator.vibrate([250, 50, 400]);
   });
   
   /* Fast/Short vibrate 2x */
   $("#helpUser").click(function(){
       navigator.vibrate([25, 50, 25]);
   });
   
});