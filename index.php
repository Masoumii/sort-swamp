<!DOCTYPE html>
<html lang="en">
<head>

   <?php require("header.php");?>

       <Script>

 /* Check Network Status ( Online or Offline) */
 function updateNetworkStatus() {

            var networkStatus = navigator.onLine ? 'online' : 'offline';

            /* If network disconnected and/or offline, disable score submit */
            if(networkStatus === "offline"){
                $("<div class='animated pulse infinite' id='offline-msg'>You are offline and cannot submit your score!</div>").appendTo($('body')).delay(5000).slideUp(1000);
            }
      }

      </script>
      
</head>

<body onload="updateNetworkStatus()" ononline="updateNetworkStatus()" onoffline="updateNetworkStatus()">
<center>
    <div id="start-window">
        <div class="choice-section">
        <h1 id="start-game" class="start-text">START GAME</h1>
        <br>
        <h1 id="hall-of-fame" class="start-text">HALL OF FAME</h1>
        </div>
        
        <div class="info-section">
            <h1><img alt="Explanation about the wipe function" width="40" src="img/clean.svg">&nbsp;&nbsp;&nbsp;Wipe the sorting screen if you've made a mistake </h1>
            <h1><img alt="Explanation about the thunder function" width="40" src="img/lightning.svg">&nbsp;&nbsp;&nbsp;Zap the animals and win the round if you're stuck! </h1>
            <h1><img alt="Explanation about the help function" width="40" src="img/help.svg">&nbsp;&nbsp;&nbsp;Call for help to remind you the sorting order (<span style='color:red'> -5 sec </span>)</h1>
            <br>
            <br>
            <h1><img width="40" src="img/money.svg">&nbsp;&nbsp;&nbsp;The Top 3 players win great money prices every month!</h1>
            <hr>
        </div>
    </div>

    <div id="winner-window">
        <h1 class="winner-text">YOU WON!</h1>
    </div>
    
    <div id="gameover-window">
        <h1 class='gameover-text'>GAME OVER!</h1>
      <form id="score-form" method="POST" action="insert_scoreboard.php">
            <input name="name" type="text" placeholder="Name" required>
            <br>
            <input name="email" type="email" placeholder="Email" required>
            <input id="score" name="score" type="hidden" value="">
            <br><br>
            <input class="animated pulse infinite" type="submit" name="submitScore" value="SUBMIT SCORE">
            <br>
        </form>
        <br>
       
    </div>
    </center>

    
    <center><img alt="Winner animation" class="winner-img" width="175" src="img/winner.gif"> </center>
            <!-- BUTTONS -->
            <div id="images">
            <img alt="Crab" src="img/crab.svg" class="animals" id="newCrab">
            <img alt="Fish" src="img/whale.svg" class="animals" id="newFish">
            <img alt="Cat" src="img/cat.svg" class="animals" id="newCat">
            <img alt="Fox" src="img/fox.svg" class="animals" id="newFox">
            
             <img title="Explode!" alt="Explode" src="img/lightning.svg" class="animals" id="explode">
             <img title="-5 Seconds" alt="Help" src="img/help.svg" class="animals" id="helpUser">
             <img title="Wipe clean"  alt="Clean" src="img/clean.svg" class="animals" id="clearWindow">
            
           
            <span id="time"></span>
            <br><br><br>
            </div>
           
    <!-- CONTAINER WAAR DIEREN IN VALLEN -->
    <div class="container"></div>

    <!-- CONTAINER OM DE VOLGORDE TE LATEN ZIEN-->
    <div id="example"></div>


    


</body>

</html>