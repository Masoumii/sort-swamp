<?php

    $counter = 0;

   require("header.php");
   require("dbconfig.php");
   
/* PULL FROM DATABASE */
    $conn = DatabaseConnection::getConnection();
    $userId = $_SESSION['id'];
    $q = "SELECT id,name,score FROM sortfort_scoreboard WHERE score >= 10 ORDER BY score DESC, timestamp DESC LIMIT 50;";
    $stmt = $conn->prepare( $q );
    $stmt->execute();
    ?>

                <table>
                <tr>
                <th style="width:25%;font-weight:normal;">#</th>
                <th style="width:100%">Name</th>
                <th class="score-col">Score</th>
                </tr>
                
    <?php
    while($row = $stmt->fetch()){
        $counter += 1;
        $name = $row['name'];
        $score = $row['score'];
        $email = $row['email'];
        
        ?>   
                <tr>
                    <td><?=$counter?></td>
                    <td><?=$name?></td>
                    <td><?=$score?></td>
                </tr> 
        <?php
    }
    ?>
                </table>
<div class='price-section'>
    <h1>Prices</h1>
    <hr>
    <h1 class="animated pulse infinite">#1   <span style="color:#019688"><img width="40" src="img/money.svg">&nbsp;&euro;30</span></h1>
    <h1 class="animated pulse infinite">#2   <span style="color:#019688"><img width="40" src="img/money.svg">&nbsp;&euro;20</span></h1>
    <h1 class="animated pulse infinite">#3   <span style="color:#019688"><img width="40" src="img/money.svg">&nbsp;&euro;10</span></h1>
    <hr>
</div>

<br>
<br>

<a class="back-link" href="#">BACK</a>